# pre-diploma-generation-svc

Pre Diploma Practice Part

Service of generation faces from resource `thispersondoesnotexist` 

To run project 

```bash

python3 generate.py 100 
# Where 100 is amount of images which should be generated

```

Then will be created `assets` folder with content
First will be created folder `assets/faces` - where will be downloaded all content
then in this folder `assets/faces-resized` - content will be reduced to 320x320 format
and `assets/faces-blurred-{level}` - content will be blurred by level
