import cv2
import numpy as np
import os
import sys


def resize(image_for_resize):
    return cv2.resize(image_for_resize, (320, 320))


def create_directory_if_not_exists(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def load_and_resize_images():
    faces_folder = 'assets/faces'
    faces_resized_folder = 'assets/faces-resized'
    create_directory_if_not_exists('assets')
    create_directory_if_not_exists(faces_folder)
    create_directory_if_not_exists(faces_resized_folder)
    images = []
    os.system('sh scripts/downloadThisPersonDoesntExist.sh {amount}'.format(amount=int(sys.argv[1])))
    for i in range(1, len(os.listdir(faces_folder)) + 1):
        image = cv2.imread('{folder}/{number}.jpg'.format(folder=faces_folder, number=i), flags=cv2.IMREAD_COLOR)
        image_resized = resize(image)
        cv2.imwrite('{folder}/{number}.jpg'.format(folder=faces_resized_folder, number=i), image_resized)
        images.append(image_resized)
    return images


def blur_images_and_move_to_folder(images, level):
    path_to_folder = 'assets/faces-blurred-{level}'.format(level=level)
    create_directory_if_not_exists(path_to_folder)

    for i, image in enumerate(images):
        image_blurred = cv2.blur(src=image, ksize=(level, level))
        cv2.imwrite('{folder}/{number}.jpg'.format(folder=path_to_folder, number=i+1), image_blurred)
    print('Generated images for {level} level'.format(level=level))


if __name__ == "__main__":
    images_resized = load_and_resize_images()
    blur_images_and_move_to_folder(images_resized, 5)
    blur_images_and_move_to_folder(images_resized, 10)
    blur_images_and_move_to_folder(images_resized, 20)
    blur_images_and_move_to_folder(images_resized, 30)
    blur_images_and_move_to_folder(images_resized, 40)
    blur_images_and_move_to_folder(images_resized, 50)
    blur_images_and_move_to_folder(images_resized, 60)
